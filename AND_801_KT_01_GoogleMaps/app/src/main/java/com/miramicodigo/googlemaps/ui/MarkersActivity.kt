package com.miramicodigo.googlemaps.ui

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import com.miramicodigo.googlemaps.R

class MarkersActivity : AppCompatActivity() , OnMapReadyCallback, GoogleMap.OnMarkerClickListener{

    lateinit var marcador: Marker

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_markers)

        title = "Markers"

        val mapFragment = supportFragmentManager.findFragmentById(R.id.mapMarkers) as SupportMapFragment
        mapFragment.getMapAsync(this)
    }

    override fun onMapReady(googleMap: GoogleMap) {

    }

    override fun onMarkerClick(marker: Marker): Boolean {
        if (marker == marcador) {
            Toast.makeText(applicationContext, "LatLng: " + marker.position.latitude + "," +
                    marker.position.longitude, Toast.LENGTH_LONG).show()
        }
        return false
    }
}
